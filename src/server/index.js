const csv = require("csvtojson");
const fs = require("fs");

const csvMatchesFilePath = "./src/data/matches.csv";
const csvDeliveriesPath = "./src/data/deliveries.csv";

const iplFn = require("./ipl.js");

const csvToJson = (csvMatchesFilePath, csvDeliveriesPath) => {
  csv()
    .fromFile(csvMatchesFilePath)
    .then((matches) => {
      csv()
        .fromFile(csvDeliveriesPath)
        .then((deliveries) => {
          const matchesPlayedResult = iplFn.matchesPlayedPerSeason(matches);
          let filePath1 = "./src/public/output/matchesPlayedResult.json";
          convertJson(filePath1, matchesPlayedResult);

          const matchesWonResult = iplFn.matchesWonPerYear(matches);
          filePath2 = "./src/public/output/matchesWonResult.json";
          convertJson(filePath2, matchesWonResult);

          const extraRunsResult = iplFn.extraRunIn2016(matches, deliveries);
          filePath3 = "./src/public/output/extraRunsResult.json";
          convertJson(filePath3, extraRunsResult);

          const topTenEconomicalBowlerRes = iplFn.topEconomicBowler(
            matches,
            deliveries
          );
          filePath4 = "./src/public/output/topTenEconomicBowlerRes.json";
          convertJson(filePath4, topTenEconomicalBowlerRes);

          const teamWonTossWonResult = iplFn.eachTeamTossWonMatchWon(matches);
          filePath5 = "./src/public/output/teamWonTossWon.json";
          convertJson(filePath5, teamWonTossWonResult);

          const highestNoOfPlayerOfTheMatchsRes =
            iplFn.highestPlayerOfTheMatch(matches);
          filePath6 =
            "./src/public/output/highestNoOfPlayerOfTheMatchsRes.json";
          convertJson(filePath6, highestNoOfPlayerOfTheMatchsRes);

          const topEconomicBowlerInSuperOverRes =
            iplFn.bestEconomyInSuperOver(deliveries);
          filePath7 =
            "./src/public/output/topEconomicBowlerInSuperOverRes.json";
          convertJson(filePath7, topEconomicBowlerInSuperOverRes);

          const highestPayerDismissedTimeRes =
            iplFn.highestTimePlayerDismissed(deliveries);
          filePath8 = "./src/public/output/highestPayerDismissedTimeRes.json";
          convertJson(filePath8, highestPayerDismissedTimeRes);

          const strikeRateOfBatterEachSeasonRes =
            iplFn.strikeRateOfBatterEachSeason(matches, deliveries);
          filePath9 =
            "./src/public/output/strikeRateOfBatterEachSeasonRes.json";
          convertJson(filePath9, strikeRateOfBatterEachSeasonRes);

          function convertJson(filePath, FileName) {
            fs.writeFile(filePath, JSON.stringify(FileName), (err) => {
              if (err) console.log(err);
            });
          }
        });
    });
};

csvToJson(csvMatchesFilePath, csvDeliveriesPath);
