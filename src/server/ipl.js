//Number of matches played per year for all the years in IPL.
function matchesPlayedPerSeason(matches) {
  let matchesPlayed = matches.reduce((acc, currSeason) => {
    if (acc[currSeason.season]) {
      acc[currSeason.season] += 1;
    } else {
      acc[currSeason.season] = 1;
    }
    return acc;
  }, {});
  return matchesPlayed;
}

//Number of matches won per team per year in IPL.
function matchesWonPerYear(matches) {
  let matchesWon = matches.reduce((acc, currentWon) => {
    if (acc[currentWon.season]) {
      if (acc[currentWon.season][currentWon.winner]) {
        acc[currentWon.season][currentWon.winner] += 1;
      } else {
        if (currentWon.winner != "") {
          acc[currentWon.season][currentWon.winner] = 1;
        }
      }
    } else {
      acc[currentWon.season] = {};
    }
    return acc;
  }, {});
  return matchesWon;
}

//Extra runs conceded per team in the year 2016.
function extraRunIn2016(matches, deliveries) {
  let matches_Id = matches
    .filter((match) => match.season == 2016)
    .map((match) => match.id);
  let extraRuns = deliveries.reduce((accumlator, currentRuns) => {
    let deliverId = currentRuns.match_id;
    if (matches_Id.includes(deliverId)) {
      if (accumlator[currentRuns.bowling_team]) {
        accumlator[currentRuns.bowling_team] += parseInt(
          currentRuns.extra_runs
        );
      } else {
        accumlator[currentRuns.bowling_team] = parseInt(currentRuns.extra_runs);
      }
    }
    return accumlator;
  }, {});
  return extraRuns;
}

// Top 10 economical bowlers in the year 2015
function topEconomicBowler(matches, deliveries) {
  const matchesId = matches
    .filter((match) => match.season == 2015)
    .map((match) => match.id);

  let getEconomicBowler = deliveries.reduce((accumlator, currentEconomi) => {
    let deliverId = currentEconomi.match_id;
    if (matchesId.includes(deliverId)) {
      if (accumlator[currentEconomi.bowler]) {
        accumlator[currentEconomi.bowler].ball += 1;
        accumlator[currentEconomi.bowler].total_runs += parseInt(
          currentEconomi.total_runs
        );

        let bowlersRuns = accumlator[currentEconomi.bowler].total_runs;
        let bowlersOvers = accumlator[currentEconomi.bowler].ball / 6;
        accumlator[currentEconomi.bowler].economy = bowlersRuns / bowlersOvers;
      } else {
        accumlator[currentEconomi.bowler] = {
          total_runs: parseInt(currentEconomi.total_runs),
          ball: 1,
          economy: 0,
        };
      }
    }
    return accumlator;
  }, {});

  const arrOfEconomy = [];
  for (let key_economy in getEconomicBowler) {
    arrOfEconomy.push(getEconomicBowler[key_economy].economy);
  }
  arrOfEconomy.sort(function (a, b) {
    return a - b;
  });

  const topTenEconomicBowler = {};
  for (let index = 0; index < 10; index++) {
    for (let key_bowler in getEconomicBowler) {
      if (getEconomicBowler[key_bowler].economy == arrOfEconomy[index]) {
        topTenEconomicBowler[key_bowler] =
          getEconomicBowler[key_bowler].economy;
      }
    }
  }
  return topTenEconomicBowler;
}

// Find the number of times each team won the toss and also won the match
function eachTeamTossWonMatchWon(matches) {
  let TossWonMatchWon = matches.reduce((accumlator, currentTossMatch) => {
    if (accumlator[currentTossMatch.season]) {
      if (currentTossMatch.winner == currentTossMatch.toss_winner) {
        accumlator[currentTossMatch.season] += 1;
      }
    } else {
      if (currentTossMatch.toss_winner == currentTossMatch.winner) {
        accumlator[currentTossMatch.season] = 1;
      } else {
        accumlator[currentTossMatch.season] = 0;
      }
    }

    return accumlator;
  }, {});
  return TossWonMatchWon;
}

// Find a player who has won the highest number of Player of the Match awards for each season.

function highestPlayerOfTheMatch(matches) {
  let playerOfTheMatch = matches.reduce((accumlator, currentMatch) => {
    if (accumlator[currentMatch.season]) {
      if (accumlator[currentMatch.season][currentMatch.player_of_match]) {
        accumlator[currentMatch.season][currentMatch.player_of_match] += 1;
      } else {
        accumlator[currentMatch.season][currentMatch.player_of_match] = 1;
      }
    } else {
      accumlator[currentMatch.season] = {};
    }
    return accumlator;
  }, {});

  let sortedAwardArr = Object.values(playerOfTheMatch).map((element) =>
    Object.entries(element).sort((a, b) => b[1] - a[1])
  );

  const topOfPlayerEle = sortedAwardArr.map(
    (playerElement) => playerElement[0]
  );

  let mostAwardsWinPerYear = matches.reduce((accumlator, currentMatch) => {
    accumlator[currentMatch.season] = 0;
    return accumlator;
  }, {});
  let sessionKey = Object.keys(mostAwardsWinPerYear);
  sessionKey.map((element, index) => {
    let key = topOfPlayerEle[index][0];
    let value = topOfPlayerEle[index][1];
    mostAwardsWinPerYear[element] = { [key]: value };
  });

  return mostAwardsWinPerYear;
}
// Find the bowler with the best economy in super overs
function bestEconomyInSuperOver(deliveries) {
  let economyInSuperOver = deliveries.reduce((accumlator, currentEconomy) => {
    if (accumlator[currentEconomy.bowler]) {
      if (currentEconomy.is_super_over == "1") {
        accumlator[currentEconomy.bowler].balls += 1;
        accumlator[currentEconomy.bowler].runs += parseInt(
          currentEconomy.total_runs
        );
        let bowlersRuns = accumlator[currentEconomy.bowler].runs;
        let bowlersOvers = accumlator[currentEconomy.bowler].balls / 6;
        accumlator[currentEconomy.bowler].economy = bowlersRuns / bowlersOvers;
      }
    } else {
      if (currentEconomy.is_super_over == "1") {
        accumlator[currentEconomy.bowler] = {
          runs: parseInt(currentEconomy.total_runs),
          balls: 1,
          economy: 0,
        };
      }
    }
    return accumlator;
  }, {});

  let onlyContainEconomy = Object.values(economyInSuperOver)
    .map((element) => element.economy)
    .sort((a, b) => a - b);

  let topEconomyBowlerInSuperOver = {};
  for (let key in economyInSuperOver) {
    if (economyInSuperOver[key]["economy"] == onlyContainEconomy[0]) {
      topEconomyBowlerInSuperOver[key] = onlyContainEconomy[0];
    }
  }
  return topEconomyBowlerInSuperOver;
}

//Find the highest number of times one player has been dismissed by another player.

function highestTimePlayerDismissed(deliveries) {
  let dismissalPlayer = deliveries.reduce((accumlator, currentVal) => {
    if (accumlator[currentVal.player_dismissed]) {
      if (accumlator[currentVal.player_dismissed][currentVal.bowler]) {
        accumlator[currentVal.player_dismissed][currentVal.bowler] =
          accumlator[currentVal.player_dismissed][currentVal.bowler] + 1;
      } else {
        accumlator[currentVal.player_dismissed][currentVal.bowler] = 1;
      }
    } else {
      accumlator[currentVal.player_dismissed] = {};
      accumlator[currentVal.player_dismissed][currentVal.bowler] = 1;
    }
    return accumlator;
  }, {});

  let filterPlayer = Object.keys(dismissalPlayer).filter(
    (element) => element !== ""
  );
  let sortedPlayer = filterPlayer.map((key) => {
    let sortedPlayerDismiss = Object.entries(dismissalPlayer[key]).sort(
      (a, b) => b[1] - a[1]
    );

    return [key, sortedPlayerDismiss[0]];
  });
  let sortedMostDismissPlayer = sortedPlayer.sort((a, b) => b[1][1] - a[1][1]);

  const map = new Map([sortedMostDismissPlayer[0]]);
  let highestTimePlayerDismiss = Object.fromEntries(map);
  return highestTimePlayerDismiss;
}
// Find the strike rate of a batsman for each season

function strikeRateOfBatterEachSeason(matches, deliveries) {
  let matchId = matches.map((ele) => ele.id);
  let batter_runs = deliveries.reduce((accumlator, currentVal) => {
    let delivereId = currentVal.match_id;
    if (matchId.includes(delivereId)) {
      if (accumlator[currentVal.batsman]) {
        accumlator[currentVal.batsman].batsman_runs += parseInt(
          currentVal.batsman_runs
        );
        accumlator[currentVal.batsman].ball += 1;
        accumlator[currentVal.batsman].strikeRateOfBatter = (
          (accumlator[currentVal.batsman].batsman_runs /
            accumlator[currentVal.batsman].ball) *
          100
        ).toFixed(2);
      } else {
        accumlator[currentVal.batsman] = {
          batsman_runs: parseInt(currentVal.batsman_runs),
          ball: 1,
          strikeRateOfBatter: 0,
        };
      }
    }
    return accumlator;
  }, {});
  return batter_runs;
}

module.exports = {
  matchesPlayedPerSeason,
  matchesWonPerYear,
  extraRunIn2016,
  topEconomicBowler,
  eachTeamTossWonMatchWon,
  highestPlayerOfTheMatch,
  bestEconomyInSuperOver,
  highestTimePlayerDismissed,
  strikeRateOfBatterEachSeason,
};
